// console.log('Hello World!')

// [SECTION] Syntax, Statements and Comments

// A single-line comment denoted by a slash and asterisk

/* 
This is a multi-line comment denoted by a slash and asterisk.
*/

console.log('Albert');
// JS Statements usually end with semicolon (;)

// [SECTION] Variables

// It is used to contain data
// container or storage

// Declaring Variables
// Declaring Variables tell our devices that a variable name is created and is ready to store data.

let name;
console.log(name);
// Declaring a variable without giving a value will automatically assign the value of 'undefined.'

let name1 = 'Albert';
console.log(name1);

// Declaring andn Initializing Variables
// Initializing Variables - The instance when a variable is given it's initial starting value.

// Reassigning Variable Values
let animal;
console.log(animal); /* undefined */

animal = 'cat';
console.log(animal);

animal = 'dog';
console.log(animal);

// let
let number = 'text';
let text = '12345';
console.log(number, text);

const color = 'blue';
console.log(color);

const interest = 3.456;
console.log(interest);

// const vs let
// let is possible for reassignment
// const is not

// [SECTION] Data Types

// Strings
// Series of characters that create a word, phrase, a sentence
// Strings in JS can be written using either a single ('') or double ("") quote.

let country = 'Philippines';
let province = "Metro Manila";
let myAge = 24; /* Is not a string type */
let myEdad = 'twentyfour'; /* Is a string */

console.log(country);

// Multiple Variable Declarations
console.log(province + ',' + ' ' + country);
console.log(myAge, myEdad);

// Concatenating Strings
// Multiple string values can be combined to create a single string using the '+' symbol

let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

// The escape character (\)
// '\' refers to creating a new line in between text
let mainAddress = 'Metro Manila\n\nPhillipines';
console.log(mainAddress);

let message = "John's employees went home early";
console.log(message);
message = 'John\'s employees went home early';
console.log(message);

// Numbers
// Integers/Whole Numbers
let headcount = 1_000_000_000;
console.log(headcount);

// Decimal Numbers/Fractions
let grade = 98.9;
console.log(grade);

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);

// Boolean
// Boolean values are normally used to store values relating to the state of certain things.

let isMarried = false;
let inGoodConduct = true;
console.log('isMarried: ' + isMarried);
console.log('inGoodConduct: ' + inGoodConduct);

// Arrays
// Arrays are a special kind of data type that's used to store multiple values.

// Syntax
    // let/const arrayName = [alpha, bravo, charlie];

let grades = [98.7, 92.1, 90.0, 88.4];
// array = [0, 1, 2,3 ] index value
console.log(grades);

// different data types
let details = ['John','Smith', 32, false];
console.log(details);

// Objects 
// Objects are another special kind of data type that's used to mimic real world objects/items.
// Syntax
    /* 
    let/const objectName = {
        propertyA: value,
        propertyB: value,
    }
    */

let person = {
    fullName: 'Michael Jordan',
    age: 35,
    isMarried: true,
    jerseyNumber: [23, 45, 35],
    city: ['Chicago', 'Wizards'],
    address: {
        houseNumber: '23',
        city: 'Chicago',
    }
}
console.log(person);

const myGrades = {
    firstYear: 1.67,
    secondYear: 1.05,
    thirdYear: 1.00,
    fourthYear: 1.50,
}
console.log(myGrades);

// typeof operator is used to determine the type of data

console.log(typeof myGrades);
console.log(typeof myEdad);
console.log(typeof grades);
console.log(typeof grade);
console.log(typeof isMarried);

const anime = ['one piece', 'one punch man', 'attack on titan', 'Naruto'];

anime[2] = 'bluelock';

console.log(anime);

/* 
Constant Objects and Arrays
    The keyword const is a little misleading.
    It does not define a constant value. It defines a constant reference to a value.

    Because of this we can NOT:

    Reassign a constant value
    Reassign a constant array
    Reassign a constant object

    But you CAN:

    Change the elements of constant array
    Change the properties of constant object
*/

// Null
// It's used to intentionally express the absence of a value in a variable declaration/initialization.

let spouse = null;
console.log(spouse);

// Using null compared to a 0 value and an empty string is much better for reading purposes.

let myNumber = 0;
let myString = '';

console.log(myNumber);
console.log(myString);

// Undefined
let fullName;
console.log(fullName);

// Undefined vs Null

// null means that a variable was created and was assigned a value that doesnt hold any amount/value

let varA = null;
console.log(varA);

// This is when the value of a variable is still unknown.
let varB;
console.log(varB);